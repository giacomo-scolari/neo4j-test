package spring.data.neo4j.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import spring.data.neo4j.domain.Path;
import spring.data.neo4j.domain.Subject;
import spring.data.neo4j.mappers.SubjectMapper;
import spring.data.neo4j.resources.SubjectResource;
import spring.data.neo4j.services.SubjectService;

import java.util.ArrayList;
import java.util.List;

@RestController
public class SubjectController {

    private final SubjectService service;

    private final SubjectMapper mapper;

    @Autowired
    public SubjectController(SubjectService service, SubjectMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @GetMapping("/subject/{subjectId}")
    public SubjectResource findBySubjectId(@PathVariable Long subjectId) {
        return mapper.toResource(service.findBySubjectId(subjectId));

    }

    @GetMapping("/subject")
    public Iterable<SubjectResource> findBy(
            @RequestParam(value = "cd_fiscale", required = false) String taxCode,
            @RequestParam(value = "partita_iva", required = false) String vatCode,
            @RequestParam(value = "denominazione", required = false) String fullName
    ) {
        Iterable<Subject> subjects;
        if (taxCode != null) {
            subjects = service.findByTaxCode(taxCode);
        } else if (vatCode != null) {
            subjects = service.findByVatCode(vatCode);
        } else if (fullName != null) {
            subjects = service.findByFullNameLike(fullName);
        } else {
            subjects = new ArrayList<>();
        }
        return mapper.toResource(subjects);
    }

    @GetMapping("/subjects")
    public Iterable<SubjectResource> findFirstSubjects(@RequestParam(value = "limit", required = false) Integer limit) {
        return mapper.toResource(service.findFirstSubjects(limit == null ? 100 : limit));
    }

    @GetMapping("/shortest_paths")
    public Iterable<Path> getShortestPaths(
            @RequestParam(value = "id_soggetto") List<Long> subjectIds,
            @RequestParam(value = "salti", required = false, defaultValue = "3") Integer hops
    ) {
        return service.getShortestPaths(subjectIds, 100, hops);
        //return new GraphResource(mapper.toResource(service.getShortestPaths(subjectIds, 100, hops)));
    }
}