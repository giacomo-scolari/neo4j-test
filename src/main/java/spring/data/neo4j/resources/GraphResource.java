package spring.data.neo4j.resources;

public class GraphResource {

    private Iterable<SubjectResource> nodes;

    public GraphResource(Iterable<SubjectResource> nodes) {
        this.nodes = nodes;
    }

    public Iterable<SubjectResource> getNodes() {
        return nodes;
    }

    public void setNodes(Iterable<SubjectResource> nodes) {
        this.nodes = nodes;
    }
}
