package spring.data.neo4j.resources;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SubjectResource {

    @JsonProperty("id_soggetto")
    private Long subjectId;

    @JsonProperty("cd_fiscale")
    private String taxCode;

    @JsonProperty("partita_iva")
    private String vatCode;

    @JsonProperty("denominazione")
    private String fullName;

    @JsonProperty("nome")
    private String firstName;

    @JsonProperty("cognome")
    private String lastName;

    public Long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Long subjectId) {
        this.subjectId = subjectId;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public String getVatCode() {
        return vatCode;
    }

    public void setVatCode(String vatCode) {
        this.vatCode = vatCode;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
