package spring.data.neo4j.repositories;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;
import spring.data.neo4j.domain.Subject;

/**
 * @see <a href="https://docs.spring.io/spring-data/neo4j/docs/current/reference/html/#_query_methods">
 *     Query Methods
 *     </a>
 */
public interface SubjectRepository extends Neo4jRepository<Subject, Long>, ShortestPathsRepository {

    Subject findBySubjectId(@Param("id_soggetto") Long subjectId);

    Iterable<Subject> findByTaxCode(@Param("cd_fiscale") String taxCode);

    Iterable<Subject> findByVatCode(@Param("partita_iva") String vatCode);

    Iterable<Subject> findByFullNameLike(@Param("denominazione") String fullName);

    @Query("MATCH (n:soggetto) RETURN n LIMIT $limit")
    Iterable<Subject> findFirstSubjects(@Param("limit") int limit);

}
