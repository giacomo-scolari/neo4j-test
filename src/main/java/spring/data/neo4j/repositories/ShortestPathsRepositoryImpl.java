package spring.data.neo4j.repositories;

import org.neo4j.ogm.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import spring.data.neo4j.domain.Path;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShortestPathsRepositoryImpl implements ShortestPathsRepository {

    private final Session session;

    @Autowired
    public ShortestPathsRepositoryImpl(Session session) {
        this.session = session;
    }

    public Iterable<Path> getShortestPaths(
            @Param("subjectIds") List<Long> subjectIds,
            @Param("limit") int limit,
            @Param("hops") int hops) {

        String query = MessageFormat.format("MATCH p_sp = (n:soggetto)" +
                "  WHERE n.id_soggetto IN $subjectIds" +
                "  WITH n AS nodes LIMIT $limit WITH collect(nodes) AS nodesList" +
                "  UNWIND nodesList AS groupa UNWIND nodesList AS groupb" +
                "  WITH groupa, groupb WHERE id(groupa) <= id(groupb)" +
                "  OPTIONAL MATCH p = allShortestPaths((groupa)-[*..{0}]-(groupb))" +
                "  RETURN nodes(p) as nodes, rels(p) as rels;", hops);

        Map<String, ?> parameters = new HashMap<String, Object>() {{
            put("subjectIds", subjectIds);
            put("limit", limit);
        }};

        return session.query(Path.class, query, parameters);
    }
}
