package spring.data.neo4j.repositories;

import org.springframework.data.repository.query.Param;
import spring.data.neo4j.domain.Path;

import java.util.List;

public interface ShortestPathsRepository {

    Iterable<Path> getShortestPaths(
            @Param("subjectIds") List<Long> subjectIds,
            @Param("limit") int limit,
            @Param("hops") int hops);
}
