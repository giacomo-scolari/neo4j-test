package spring.data.neo4j.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import spring.data.neo4j.domain.Path;
import spring.data.neo4j.domain.Subject;
import spring.data.neo4j.repositories.SubjectRepository;

import java.util.List;

@Service
public class SubjectService {

    private final SubjectRepository repository;

    @Autowired
    public SubjectService(SubjectRepository repository) {
        this.repository = repository;
    }

    @Transactional(readOnly = true)
    public Subject findBySubjectId(Long subjectId) {
        return repository.findBySubjectId(subjectId);
    }

    @Transactional(readOnly = true)
    public Iterable<Subject> findByTaxCode(String taxCode) {
        return repository.findByTaxCode(taxCode);
    }

    @Transactional(readOnly = true)
    public Iterable<Subject> findByVatCode(String vatCode) {
        return repository.findByVatCode(vatCode);
    }

    @Transactional(readOnly = true)
    public Iterable<Subject> findByFullNameLike(String fullName) {
        return repository.findByFullNameLike(fullName);
    }

    @Transactional(readOnly = true)
    public Iterable<Subject> findFirstSubjects(int limit) {
        return repository.findFirstSubjects(limit);
    }

    @Transactional(readOnly = true)
    public Iterable<Path> getShortestPaths(List<Long> subjectIds, int limit, int hops) {
        return repository.getShortestPaths(subjectIds, limit, hops);
    }

}
