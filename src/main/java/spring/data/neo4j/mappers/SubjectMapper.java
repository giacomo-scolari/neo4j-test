package spring.data.neo4j.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import spring.data.neo4j.domain.Subject;
import spring.data.neo4j.resources.SubjectResource;

import java.util.Collection;

@Mapper(componentModel = "spring")
public interface SubjectMapper {

    SubjectMapper INSTANCE = Mappers.getMapper( SubjectMapper.class );

    SubjectResource toResource(Subject subject);

    Subject fromResource(SubjectResource subjectResource);

    Iterable<SubjectResource> toResource(Iterable<Subject> subjects);

    Iterable<Subject> fromResource(Iterable<SubjectResource> subjects);
}


