package spring.data.neo4j.domain;

import org.springframework.data.neo4j.annotation.QueryResult;

import java.util.List;

@QueryResult
public class Path {

    private List<Subject> nodes;

    public List<Subject> getNodes() {
        return nodes;
    }

    public void setNodes(List<Subject> nodes) {
        this.nodes = nodes;
    }
}
