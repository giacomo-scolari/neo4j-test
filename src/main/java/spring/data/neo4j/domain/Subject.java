package spring.data.neo4j.domain;

import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Property;
import org.springframework.data.annotation.Id;

@NodeEntity(label = "soggetto")
public class Subject {

    @Id
    private Long id;

    @Property(name = "id_soggetto")
    private Long subjectId;

    @Property(name = "cd_fiscale")
    private String taxCode;

    @Property(name = "partita_iva")
    private String vatCode;

    @Property(name = "denominazione")
    private String fullName;

    @Property(name = "nome")
    private String firstName;

    @Property(name = "cognome")
    private String lastName;

    public Subject() {

    }

    public Long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Long subjectId) {
        this.subjectId = subjectId;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public String getVatCode() {
        return vatCode;
    }

    public void setVatCode(String vatCode) {
        this.vatCode = vatCode;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
