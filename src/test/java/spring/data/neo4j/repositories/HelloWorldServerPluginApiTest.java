package spring.data.neo4j.repositories;

import com.graphaware.test.integration.ServerIntegrationTest;
import com.graphaware.test.unit.GraphUnit;
import org.junit.Before;
import org.junit.Test;
import org.neo4j.graphdb.*;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

import static org.junit.Assert.*;

public class HelloWorldServerPluginApiTest extends ServerIntegrationTest {

    @Override
    protected String configFile() {
        return "test-neo4j.conf";
    }

    @Test
    public void shouldCreateAndReturnNode() {
        String result = httpClient.get(baseNeoUrl() + "/db/data/labels", 200);
        assertTrue(result.contains("Foo"));
        assertTrue(result.contains("Bar"));
        GraphUnit.assertSameGraph(
                getDatabase(),
                "CREATE (n:Foo), (m:Bar)");
    }
}